package components;

import com.beust.ah.A;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.BankPage;
import pages.IpotekaPage;
import pages.KartlarPage;

import java.time.Duration;

public class MainHeaderComponent extends AbsBaseComponent{
    public MainHeaderComponent(WebDriver driver) {
        super(driver);
    }
    @FindBy(css="span>a[href=\"https://abb-bank.az/az/ferdi/kartlar\"]")
    private WebElement cards;

    public KartlarPage chooseCards() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", cards);
        return new KartlarPage(driver);
    }
    @FindBy(css="span>a[href=\"https://abb-bank.az/az/ferdi/bank-24-7\"")
    private WebElement bank;

    public BankPage chooseBank() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", bank);
        return new BankPage(driver);
    }
    @FindBy(css="span>a[href=\"https://abb-bank.az/az/ferdi/kreditler/ipoteka-kreditleri\"")
    private WebElement mortgage;

    public void chooseIpoteka() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", mortgage);
        System.out.println("Clicked...");
    }
    @FindBy(css="#js-header-s3 > li:nth-child(2) > div > div.col-lg-5.col-xl-4.p-0 > div > span:nth-child(1) > a")
    private WebElement daxiliIpotekaKrediti;

    public IpotekaPage chooseDaxiliIpotekaKrediti() {
        chooseIpoteka();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", daxiliIpotekaKrediti);
        System.out.println("Clicked...");
        return new IpotekaPage(driver);
    }

}
