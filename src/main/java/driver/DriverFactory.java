package driver;

import driver.implementations.ChromeWebDriver;
import driver.implementations.EdgeWebDriver;
import driver.implementations.FirefoxWebDriver;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.Locale;

public class DriverFactory implements IDriverFactory{
    private String browserType = System.getProperty("browser").toLowerCase(Locale.ROOT);


    @Override
    public WebDriver getDriver() throws DriverNotSupportedException {
        switch (this.browserType) {
            case "chrome": {
                return new ChromeWebDriver().newDriver();
            }
            case "firefox": {
                return new FirefoxWebDriver().newDriver();
            }
            case "edge": {
                return new EdgeWebDriver().newDriver();
            }
            default:
                throw new DriverNotSupportedException(this.browserType);
        }

    }
}
