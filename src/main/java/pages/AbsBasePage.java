package pages;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageobject.AbsPageObject;
public abstract class AbsBasePage extends AbsPageObject {
    private final static String BASE_URL = System.getProperty("base.url");
    public AbsBasePage(WebDriver driver) {
        super(driver);
    }
    public void open(String path) {
        driver.get(BASE_URL + path);
    }
    public String getTitle() {
        return driver.getTitle();
    }
    public void isTitleTheSameAs(String title) {
        Assert.assertEquals(title, getTitle(), "Titles do not match with each other");
    }
}
