package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.Set;
public class BankPage extends AbsBasePage {
    public BankPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(css = "#js-hover-link > span:nth-child(1) > a")
    private WebElement abbMobileBank;
    @FindBy(css = "h1")
    private WebElement header;
    public void clickAbbMobileBank() {
        abbMobileBank.click();
    }
    public void isHeaderTheSameAs(String header) {
        clickAbbMobileBank();
        String mainWindowHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        String newWindowHandle = windowHandles.stream()
                .filter(handle -> !handle.equals(mainWindowHandle))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("New window not found."));

        driver.switchTo().window(newWindowHandle);

        System.out.println("Switched....");

        Assert.assertTrue(this.header.isDisplayed(), "Header is not displayed in the new window");
        Assert.assertEquals(header, this.header.getText(), "Header's text is not the same as expected");

    }

}