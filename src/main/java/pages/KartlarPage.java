package pages;

import data.CardData;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;
public class KartlarPage extends AbsBasePage{
    public KartlarPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(css=".container.pt-4.my-lg-4>.row.mb-4.d-flex>a>div>h3")
    private List<WebElement> cardsNames;
    @FindBy(css="h1")
    private WebElement cardTitle;
    public void chooseCardByName(CardData data) {
        for(WebElement cardName : cardsNames) {
            if(cardName.getText().equalsIgnoreCase(data.getName())) {
                cardName.click();
                System.out.println("Clicked...");
                Assert.assertEquals(data.getName(), cardTitle.getText(), "Correct card is not chosen");
                break;
            }
        }

    }
}



