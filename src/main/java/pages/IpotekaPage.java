package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class IpotekaPage extends AbsBasePage{
    public IpotekaPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(css="#from_abb_no")
    private WebElement xeyrInput;
    public void isXeyrChosen() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click()", xeyrInput);
        System.out.println("Clicked...");
        if(xeyrInput.isSelected()) {
            System.out.println("Xeyr input is selected");
        } else {
            System.out.println("Xeyr input is not selected");
        }
        Assert.assertTrue(xeyrInput.isSelected(), "Xeyr input is not selected");
    }
}
