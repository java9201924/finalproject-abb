package data;

public enum CardData {

    MasterCardDebet("TamKart MasterCard Debet"),
    VISADebet("TamKart VISA Classic Debet"),
    TamGencVISA("TamGənc VISA Debet"),
    TamEcoVISA("TamEco VISA Classic Debet"),
    TamDigiCard("Tam DigiCard"),

    VISAClassic("TamKart VISA Classic"),
    MasterCardStandard("TamKart MasterCard Standard"),
    VISAPlatinum("TamKart VISA Platinum"),
    VISAPremium("TamKart VISA Premium"),
    VISACredit("ABB Miles VISA Kredit");
    private String name;

    CardData(String name) {
        this.name = name;
    }
    public String getName() {
        return this.name;
    }

}
