import components.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;

public class BankPageTest {

    private WebDriver driver;
    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void checkHeaderInNewWindow()  {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseBank()
                .isHeaderTheSameAs("ABB mobile – Sadə və sürətli");
    }
    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
