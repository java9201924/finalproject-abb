import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;
public class MainPageTest {
    private WebDriver driver;
    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void checkTitleOfThePage() {
        new MainPage(driver)
                .open("");
        new MainPage(driver)
                .isTitleTheSameAs("ABB - Müasir, Faydalı, Universal");
    }
    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
