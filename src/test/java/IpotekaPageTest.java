import components.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.reporters.jq.Main;
import pages.MainPage;

public class IpotekaPageTest {
    private WebDriver driver;
    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void checkXeyrIsChosen() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseDaxiliIpotekaKrediti()
                .isXeyrChosen();
    }
    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
