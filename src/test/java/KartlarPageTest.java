import components.MainHeaderComponent;
import data.CardData;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;
public class KartlarPageTest {
    private WebDriver driver;
    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }
    @Test
    public void checkTitleOfThePage() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseCards()
                .isTitleTheSameAs("Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları");
    }
    @Test
    public void checkCorrectCardIsSelected() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseCards()
                .chooseCardByName(CardData.MasterCardDebet);
    }
    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
